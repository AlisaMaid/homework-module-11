#include <iostream>

//Calling std::cout function
void print()
{
    std::cout << "Hello Skillbox!\n";
}

/*
    This is a 
    multi-line comment
*/

int main()
{
    int x = 100;
    int y = x + 100;
    int mult = x * y;

    int b = 0;
    b = b + 2;

    int test;
    test = 100500;

    int test2 = 100501;

    int random = 1002;

    std::cout << random;

}

